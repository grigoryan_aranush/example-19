﻿using System;
class Example
{
    static void Main()
    {
        double x, y;
        int i;
        byte b;
        short s;
        uint u;


        /*
        char ch;
        long l;
        */
        x = 10.3;
        y = 3.0;
        // Cast double to int, fractional component is lost
        i = (int)(x / y);
        Console.WriteLine("The integer component of x/y is: " + i);

        i = 255;
        //cast i to byte, no data lost.
        b = (byte)i;
        Console.WriteLine("b is: " + b);

        //Cast 258 to byte, data lost

        i = 258;
        b = (byte)i;
        Console.WriteLine("data lost " + b);


        u = 32000;
        s = (short)u;
        //Cast into short, no data lost
        Console.WriteLine(s);


        //Cast a uint into short, data lost
        u = 64000;
        s = (short)u;
        Console.WriteLine(s);

    }
}